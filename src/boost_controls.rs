/// Represents how much boost to apply. If `elapsed < left * current_half_life` then no boost is
/// applied. If `elapsed > right * current_half_life` then full boost is applied. For the values
/// inside the interval it is scaled linearly.
#[derive(Clone, Copy, Debug)]
pub struct BoostControls {
    pub(crate) left: f64,
    pub(crate) right: f64,
}

impl BoostControls {
    /// Constrcuts new [`BoostControls`] from the given interval. If `left` is not positive and
    /// finite or if `right` is NaN or not greater than `left`, then [`None`] is returned.
    pub fn new(left: f64, right: f64) -> Option<Self> {
        if left.is_finite() && left >= 0.0 && !right.is_nan() && left < right {
            Some(BoostControls { left, right })
        } else {
            None
        }
    }

    /// Returns the left margin of the interval.
    pub fn left(&self) -> f64 {
        self.left
    }

    /// Returns the right margin of the interval.
    pub fn right(&self) -> f64 {
        self.right
    }
}

impl Default for BoostControls {
    fn default() -> Self {
        Self {
            left: 0.3,
            right: 1.0,
        }
    }
}
