use crate::{logsumexp, Outcome, Quiz};
use scilib::math::bessel::k;
use statrs::{
    distribution::Gamma,
    function::{
        beta::ln_beta,
        gamma::{gamma, ln_gamma},
    },
};
use std::f64::consts::LN_2;

#[derive(Debug, PartialEq)]
pub struct GammaUpdate {
    pub gamma: Gamma,
    pub mean: f64,
}

impl GammaUpdate {
    pub fn new(dist: Gamma, quiz: &Quiz) -> Self {
        match quiz.outcome {
            Outcome::Binomial { successes, total } => {
                let logmoment = |nth| {
                    let failures = total - successes;
                    let loglik = (0..=failures)
                        .map(|i| {
                            Self::binomln(failures as f64, i as f64)
                                + Self::int_gamma_pdf_exp(
                                    dist.shape() + nth as f64,
                                    dist.rate(),
                                    quiz.elapsed * (successes + i) as f64,
                                    true,
                                )
                        })
                        .collect::<Vec<_>>();
                    let scales = (0..=failures)
                        .map(|i| (-1.0_f64).powi(i as i32))
                        .collect::<Vec<_>>();
                    logsumexp(loglik, scales)
                };

                let logm0 = logmoment(0);
                let logmean = logmoment(1) - logm0;
                let logm2 = logmoment(2) - logm0;
                let logvar = logsumexp(vec![logm2, 2.0 * logmean], vec![1.0, -1.0]);
                let gamma =
                    Gamma::new((2.0 * logmean - logvar).exp(), (logmean - logvar).exp()).unwrap();
                GammaUpdate {
                    gamma,
                    mean: logmean.exp(),
                }
            }
            Outcome::Noisy(result) => {
                let moment = |n| {
                    let an = dist.shape() + n;
                    Self::int_gamma_pdf_exp(an, dist.rate(), quiz.elapsed, false)
                        * (2.0 * result - 1.0)
                        + (1.0 - result) * gamma(an) / dist.rate().powf(an)
                };
                let m0 = moment(0.0);
                let mean = moment(1.0) / m0;
                let m2 = moment(2.0) / m0;
                let var = m2 - mean.powi(2);
                let gamma = Gamma::new(mean.powi(2) / var, mean / var).unwrap();
                GammaUpdate { gamma, mean }
            }
        }
    }

    fn binomln(n: f64, k: f64) -> f64 {
        -ln_beta(1.0 + n - k, 1.0 + k) - (n + 1.0).ln()
    }

    fn int_gamma_pdf(a: f64, b: f64, log_domain: bool) -> f64 {
        if !log_domain {
            b.powf(-a) * gamma(a)
        } else {
            -a * b.ln() + ln_gamma(a)
        }
    }

    fn int_gamma_pdf_exp(a: f64, b: f64, c: f64, log_domain: bool) -> f64 {
        if c == 0.0 {
            Self::int_gamma_pdf(a, b, log_domain)
        } else {
            let z = 2.0 * (b * c).sqrt();
            if !log_domain {
                2.0 * (c / b).powf(a * 0.5) * k(z, a).re
            } else {
                LN_2 + (c / b).ln() * (a * 0.5) + (k(z, a).re * z.exp()).ln() - z
            }
        }
    }
}
