//! A Rust port of the Ebisu v3 flashcard quiz scheduling library.
//!
//! # Usage example
//!
//! ```
//! use ebirsu::{Model, Outcome, Quiz};
//! use statrs::distribution::Gamma;
//!
//! // Initialize the model with the first argument representing the half-life of the recall.
//! // It is 10 units of time (minutes, hours, days, etc.), meaning after 10 units of time have
//! // passed the probability of recall is 0.5. The second argument specifies how much boost to
//! // apply after a successful update.
//! let mut model = Model::from_priors(Gamma::new(1.0, 0.1).unwrap(), Gamma::new(4.5, 3.0).unwrap()).unwrap();
//! // Updates the recall with a quiz that took place 2 units of time after the creaton of the
//! // model where the user had 2 correct responses out of 3.
//! model.update_recall(
//!     Quiz::new(
//!         2.0,
//!         Outcome::Binomial {
//!             successes: 2,
//!             total: 3,
//!         },
//!     ).unwrap(),
//!     Default::default(),
//!     Default::default(),
//! );
//! // Updates the recall with a quiz that took place 1 unit of time after the last update where the
//! // user had 1 correct response out of 1.
//! model.update_recall(
//!     Quiz::new(
//!         1.0,
//!         Outcome::Binomial {
//!             successes: 1,
//!             total: 1,
//!         },
//!     ).unwrap(),
//!     Default::default(),
//!     Default::default(),
//! );
//! // Updates the recall with a quiz that took place 1 unit of time after the last update where the
//! // user responded correctly 70% of the time.
//! model.update_recall(
//!     Quiz::new(2.5, Outcome::Noisy(0.7)).unwrap(),
//!     Default::default(),
//!     Default::default(),
//! );
//! // Updates the recall to be more accurate. This can be called as often as needed, even after
//! // every quiz update, but it is a more expensive operation.
//! model.update_recall_history(Default::default());
//! let recall_probability = model.predict_recall_ln(3.0).exp();
//! ```

#![warn(missing_docs)]

mod boost_controls;
mod gamma_update;
mod logsumexp;
mod model;
mod outcome;
mod quiz;
mod reinforcement;

pub use boost_controls::BoostControls;
pub(crate) use gamma_update::GammaUpdate;
pub(crate) use logsumexp::logsumexp;
pub use model::Model;
pub use outcome::Outcome;
pub use quiz::Quiz;
pub use reinforcement::Reinforcement;
