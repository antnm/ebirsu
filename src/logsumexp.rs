pub fn logsumexp(a: Vec<f64>, b: Vec<f64>) -> f64 {
    let amax = a.iter().copied().fold(f64::NAN, f64::max);
    a.iter()
        .copied()
        .zip(b.iter().copied())
        .map(|(a, b)| (a - amax).exp() * b)
        .sum::<f64>()
        .ln()
        + amax
}
