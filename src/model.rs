use crate::{logsumexp, BoostControls, GammaUpdate, Outcome, Quiz, Reinforcement};
use ndarray::{Array, Array1, Axis};
use ndarray_linalg::LeastSquaresSvd;
use ndarray_rand::{rand_distr::Uniform, RandomExt};
use ndarray_stats::QuantileExt;
use statrs::{
    distribution::{Continuous, ContinuousCDF, Gamma},
    statistics::Distribution,
};
use std::f64::consts::LN_2;

/// This struct is used to model a single fact including all its past quizes.
#[derive(Clone, Debug)]
pub struct Model {
    quizes: Vec<Quiz>,
    init_hl_prior: Gamma,
    boost_prior: Gamma,
    init_hl: Gamma,
    boost: Gamma,
    current_half_life: f64,
    strength_ln: f64,
}

impl Model {
    /// Constructs a new model from previously calculated data. Returns [`None`] if either
    /// `current_half_life` or `strength_ln` is not finite or if `current_half_life` is not
    /// positive.
    pub fn new(
        quizes: &[Quiz],
        init_hl_prior: Gamma,
        boost_prior: Gamma,
        init_hl: Gamma,
        boost: Gamma,
        current_half_life: f64,
        strength_ln: f64,
    ) -> Option<Self> {
        if current_half_life.is_finite() && current_half_life > 0.0 && strength_ln.is_finite() {
            Some(Self {
                quizes: quizes.into(),
                init_hl_prior,
                boost_prior,
                init_hl,
                boost,
                current_half_life,
                strength_ln,
            })
        } else {
            None
        }
    }

    /// Creates a new model given the initial half-life prior and the boost prior. Returns [`None`]
    /// if the mean of the initial half-life prior is not positive or if the mean of the boost is
    /// less than 1.
    pub fn from_priors(init_hl_prior: Gamma, boost_prior: Gamma) -> Option<Self> {
        let current_half_life = init_hl_prior.mean().unwrap();
        if current_half_life > 0.0 && boost_prior.mean().unwrap() >= 1.0 {
            Some(Self {
                quizes: Vec::new(),
                init_hl_prior,
                boost_prior,
                init_hl: init_hl_prior,
                boost: boost_prior,
                current_half_life,
                strength_ln: 0.0,
            })
        } else {
            None
        }
    }

    fn current_half_life_prior(&self) -> (Gamma, f64) {
        let init_hl = self.init_hl;
        let boosted = self.current_half_life / init_hl.mean().unwrap();
        (
            Gamma::new(init_hl.shape(), init_hl.rate() / boosted).unwrap(),
            boosted,
        )
    }

    /// Updates the half-life, leaving the boost as fixed. This should be called whenever a quiz is
    /// commpleted. See also [`Self::update_recall_history`].
    pub fn update_recall(
        &mut self,
        quiz: Quiz,
        reinforcement: Reinforcement,
        boost_controls: BoostControls,
    ) {
        let (gamma_dist, total_boost) = self.current_half_life_prior();
        let updated = GammaUpdate::new(gamma_dist, &quiz);
        self.init_hl =
            Gamma::new(updated.gamma.shape(), updated.gamma.rate() * total_boost).unwrap();
        self.quizes.push(quiz);
        let boost_mean = self.boost.mean().unwrap();
        let boosted_hl = if quiz.outcome.success() {
            updated.mean
                * Self::clamp_lerp2(
                    boost_controls.left * self.current_half_life,
                    boost_controls.right * self.current_half_life,
                    1.0,
                    1.0_f64.max(boost_mean),
                    quiz.elapsed,
                )
        } else {
            updated.mean
        };
        self.current_half_life = boosted_hl;
        self.strength_ln = reinforcement.0.ln();
    }

    /// Updates both the half-life and boost. The function updates if there were at least two
    /// quizes. This operation is more expensive than the others. Calling this function is
    /// equivalent to calling
    /// [`update_recall_history_with_size`](Self::update_recall_history_with_size) with a number of
    /// samples of 10.000.
    pub fn update_recall_history(&mut self, boost_controls: BoostControls) {
        self.update_recall_history_with_size(boost_controls, 10_000)
    }

    /// Updates both the half-life and boost. The function updates if there were more than two
    /// quizes. This operation is more expensive than the others.
    pub fn update_recall_history_with_size(&mut self, boost_controls: BoostControls, size: usize) {
        if self.quizes.len() <= 2 {
            return;
        }
        let min_boost = self.boost_prior.inverse_cdf(0.01);
        let max_boost = self.boost_prior.inverse_cdf(0.9999);
        let min_half_life = self.init_hl_prior.inverse_cdf(0.01);
        let max_half_life = self.init_hl_prior.inverse_cdf(0.9999);

        let bs = Array::random(400, Uniform::new(0.0, 1.0)) * (max_boost - min_boost) + min_boost;

        let hs = Array::random(400, Uniform::new(0.0, 1.0)) * (max_half_life - min_half_life)
            + min_half_life;

        let mut posteriors = bs
            .iter()
            .copied()
            .zip(hs.iter().copied())
            .map(|(b, h)| self.posterior(b, h, boost_controls).0)
            .collect();
        let (x, y) = Self::fit_joint_to_two_gammas(&bs, &hs, &mut posteriors);
        let (x, y) = self.monte_carlo_improve(x, y, boost_controls, size);
        self.boost = x;
        self.init_hl = y;
        let bmean = x.mean().unwrap();
        let hmean = y.mean().unwrap();
        let (_, extra) = self.posterior(bmean, hmean, boost_controls);
        self.current_half_life = extra;
    }

    /// Returns the natural logarithm of the recall probability.
    pub fn predict_recall_ln(&self, elapsed: f64) -> f64 {
        -elapsed / self.current_half_life * LN_2 + self.strength_ln
    }

    /// Resets the half-life given the inital half-life prior and the natural logarithm of the
    /// strength. This also clears the quiz history.
    pub fn reset_half_life(&mut self, init_hl_prior: Gamma, strength_ln: f64) {
        self.quizes.clear();
        self.init_hl_prior = init_hl_prior;
        self.current_half_life = init_hl_prior.mean().unwrap();
        self.strength_ln = strength_ln;
    }

    fn posterior(&self, b: f64, h: f64, boost_controls: BoostControls) -> (f64, f64) {
        let logb = b.ln();
        let logh = h.ln();
        let logprior = -self.boost_prior.rate() * b - self.init_hl_prior.rate() * h
            + (self.boost_prior.shape() - 1.0) * logb
            + (self.init_hl_prior.shape() - 1.0) * logh;
        let mut loglik = 0.0;
        let mut curr_half_life = h;
        for quiz in &self.quizes {
            let log_precall = -quiz.elapsed / curr_half_life * LN_2;
            match quiz.outcome {
                Outcome::Noisy(result) => {
                    let log_pfail = (-log_precall.exp_m1()).ln();
                    if quiz.outcome.success() {
                        loglik += logsumexp(
                            vec![log_precall + result.ln(), log_pfail + (1.0 - result).ln()],
                            vec![1.0, 1.0],
                        );
                        curr_half_life *= Self::clamp_lerp2(
                            boost_controls.left * curr_half_life,
                            boost_controls.right * curr_half_life,
                            1.0,
                            b.max(1.0),
                            quiz.elapsed,
                        );
                    } else {
                        loglik += logsumexp(
                            vec![log_precall + (1.0 - result).ln(), log_pfail + result.ln()],
                            vec![1.0, 1.0],
                        );
                    }
                }
                Outcome::Binomial { .. } => {
                    if quiz.outcome.success() {
                        loglik += log_precall;
                        curr_half_life *= Self::clamp_lerp2(
                            boost_controls.left * curr_half_life,
                            boost_controls.right * curr_half_life,
                            1.0,
                            b.max(1.0),
                            quiz.elapsed,
                        );
                    } else {
                        loglik += (-log_precall.exp_m1()).ln();
                    }
                }
            }
        }
        (loglik + logprior, curr_half_life)
    }

    /// Returns the past quizes.
    pub fn quizes(&self) -> &[Quiz] {
        &self.quizes
    }

    /// Returns the initial half-life prior.
    pub fn init_hl_prior(&self) -> Gamma {
        self.init_hl_prior
    }

    /// Returns the boost prior.
    pub fn boost_prior(&self) -> Gamma {
        self.boost_prior
    }

    /// Returns the initial half-life.
    pub fn init_hl(&self) -> Gamma {
        self.init_hl
    }

    /// Returns the boost.
    pub fn boost(&self) -> Gamma {
        self.boost
    }

    /// Returns the current half-life.
    pub fn current_half_life(&self) -> f64 {
        self.current_half_life
    }

    /// Returns the natural logarithm of the strength.
    pub fn strength_ln(&self) -> f64 {
        self.strength_ln
    }

    fn clamp_lerp2(x1: f64, x2: f64, y1: f64, y2: f64, x: f64) -> f64 {
        if x <= x1 {
            y1
        } else if x >= x2 {
            y2
        } else {
            let mu = (x - x1) / (x2 - x1);
            y1 * (1.0 - mu) + y2 * mu
        }
    }

    fn fit_joint_to_two_gammas(
        x: &Array1<f64>,
        y: &Array1<f64>,
        log_posterior: &Array1<f64>,
    ) -> (Gamma, Gamma) {
        let a = ndarray::stack(
            Axis(1),
            &[
                x.mapv(f64::ln).view(),
                (x * -1.0).view(),
                y.mapv(f64::ln).view(),
                (y * -1.0).view(),
                Array::ones(x.len()).view(),
            ],
        )
        .unwrap();

        let log_posterior_max = *log_posterior.max().unwrap();

        let weights = Array::from_diag(&(log_posterior - log_posterior_max).mapv_into(f64::exp));

        let solution = weights
            .dot(&a)
            .least_squares(&weights.dot(log_posterior))
            .unwrap()
            .solution;

        (
            Gamma::new(solution[0] + 1.0, solution[1]).unwrap(),
            Gamma::new(solution[2] + 1.0, solution[3]).unwrap(),
        )
    }

    fn monte_carlo_improve(
        &self,
        xprior: Gamma,
        yprior: Gamma,
        boost_controls: BoostControls,
        size: usize,
    ) -> (Gamma, Gamma) {
        let x = Array::random(size, xprior);
        let y = Array::random(size, yprior);
        let w = x
            .iter()
            .copied()
            .zip(y.iter().copied())
            .map(|(x, y)| {
                (self.posterior(x, y, boost_controls).0 - xprior.pdf(x).ln() - yprior.pdf(y).ln())
                    .exp()
            })
            .collect::<Array1<f64>>();

        let wsum = w.sum();
        (
            Self::weighted_gamma_estimate(&w, &x, wsum),
            Self::weighted_gamma_estimate(&w, &y, wsum),
        )
    }

    fn weighted_gamma_estimate(w: &Array1<f64>, h: &Array1<f64>, wsum: f64) -> Gamma {
        let hln = h.mapv(f64::ln);
        let whdot = w.dot(h);
        let whlndot = w.dot(&hln);
        let whhlndot = (h * w).dot(&hln);
        let that2 = whhlndot / wsum - whdot / wsum * whlndot / wsum;
        let khat2 = whdot / wsum / that2;
        Gamma::new(khat2, 1.0 / that2).unwrap()
    }
}
