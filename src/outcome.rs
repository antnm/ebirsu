/// The outcome of a review for a fact in a single session.
#[derive(Clone, Copy, Debug)]
pub enum Outcome {
    /// An [`Outcome`] of a binary quiz given the number of responses that were correct out of the
    /// total number of times the fact was shown.
    Binomial {
        /// The number of responses that were correct.
        successes: u32,
        /// The total number of responses.
        total: u32,
    },
    /// An [`Outcome`] of a non-binary quiz given the success as a number
    /// between 0 and 1.
    Noisy(f64),
}

impl Outcome {
    pub(crate) fn success(self) -> bool {
        match self {
            Outcome::Binomial { successes, total } => successes >= total / 2,
            Outcome::Noisy(result) => result > 0.5,
        }
    }
}
