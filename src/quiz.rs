use crate::Outcome;

/// Represents a quiz given the time has passed since the last review of the fact and the
/// [`Outcome`] of the review. If if this is the first review of the quiz, then the last review is
/// considered when the fact was created.
#[derive(Clone, Copy, Debug)]
pub struct Quiz {
    pub(crate) elapsed: f64,
    pub(crate) outcome: Outcome,
}

impl Quiz {
    /// Constructs a quiz given the time that has passed since last review and the outcome of the
    /// review. [`None`] is returned if one of the following conditions is not true:
    /// - `elapsed` must be finite and positive
    /// - if the `outcome` is binomial, then the number of successes must be less then the total
    /// number of responses and both of these numbers must be positive
    /// - if the `outcome` is noisy, then the success is positive
    pub fn new(elapsed: f64, outcome: Outcome) -> Option<Self> {
        if match outcome {
            Outcome::Binomial { successes, total } if successes <= total => true,
            Outcome::Noisy(result) if result > 0.0 => true,
            _ => false,
        } && elapsed > 0.0
            && elapsed.is_finite()
        {
            Some(Quiz { elapsed, outcome })
        } else {
            None
        }
    }

    /// Returns the time that has passed since last review.
    pub fn elapsed(&self) -> f64 {
        self.elapsed
    }

    /// Returns the outcome of the review.
    pub fn outcome(&self) -> Outcome {
        self.outcome
    }
}
