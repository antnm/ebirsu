/// This struct allows to specify the amount of reconsolidation that has happened when
/// [`update_recall`][crate::Model::update_recall] is called. A value below 1 makes the prediction
/// more pessimistic, while a value over 1 makes it more optimistic.
#[derive(Clone, Copy, Debug)]
pub struct Reinforcement(pub(crate) f64);

impl Reinforcement {
    /// Creates a new reconsolidation value. [`None`] is returned if `value` is not positive and
    /// finite.
    pub fn new(value: f64) -> Option<Self> {
        if value.is_finite() && value > 0.0 {
            Some(Self(value))
        } else {
            None
        }
    }

    /// Returns the value of the reconsolidation.
    pub fn value(&self) -> f64 {
        self.0
    }
}

impl Default for Reinforcement {
    fn default() -> Self {
        Self(1.0)
    }
}
